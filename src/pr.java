import java.util.Scanner;

public class pr {
    public static void main(String[] args) {
        System.out.println("What would you like to order:");
        System.out.print("1. Tempura\n2. Ramen\n3. Udon\nYour order [1-3]:");
        Scanner userIn = new Scanner(System.in);
        int number = userIn.nextInt();
        userIn.close();
        if (number == 1){
            System.out.println("We have received your order for Tempura! It will be served soon!");
        }
        else if (number == 2){
            System.out.println("We have received your order for Ramen! It will be served soon!");
        }
        else if (number == 3){
            System.out.println("We have received your order for Udon! It will be served soon!");
        }
    }
}
